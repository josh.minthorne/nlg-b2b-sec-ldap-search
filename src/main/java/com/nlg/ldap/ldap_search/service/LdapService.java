package com.nlg.ldap.ldap_search.service;

import java.io.IOException;

import org.apache.directory.api.ldap.model.cursor.CursorException;
import org.apache.directory.api.ldap.model.cursor.SearchCursor;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.Response;
import org.apache.directory.api.ldap.model.message.SearchRequest;
import org.apache.directory.api.ldap.model.message.SearchRequestImpl;
import org.apache.directory.api.ldap.model.message.SearchResultEntry;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;

public class LdapService {

	public static void main(String[] args) throws LdapException, CursorException {

		try (LdapConnection connection = new LdapNetworkConnection("localhost", 10389)) {

			connection.bind();

			// Create the SearchRequest object
			SearchRequest searchRequest = new SearchRequestImpl();
			searchRequest.setScope(SearchScope.SUBTREE);
			searchRequest.addAttributes("*");
			searchRequest.setTimeLimit(5000);
			searchRequest.setBase(new Dn("ou=system"));
			searchRequest.setFilter("(cn=Jane Doe)");

			SearchCursor searchCursor = connection.search(searchRequest);

			while (searchCursor.next()) {
				Response response = searchCursor.get();

				// process the SearchResultEntry
				if (response instanceof SearchResultEntry) {
					Entry resultEntry = ((SearchResultEntry) response).getEntry();
					System.out.println("************************ We just want the password. ************************ ");
					System.out.println(resultEntry.get("userPassword"));
					System.out.println("************************ Entire entry below ************************ ");
					System.out.println(resultEntry);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public LdapService() {
	}

}
